<?php

namespace Drupal\private_message_flood\Service;

use DateInterval;
use DateTime;
use Exception;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

/**
 * Provides services for the Private Message Flood Protection module.
 */
class PrivateMessageFloodService implements PrivateMessageFloodServiceInterface {

  /**
   * The moments we distinguish.
   *
   * @var array
   *   Keys are UIDs, config arrays.
   */
  private $configData = [];

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a PrivateMessageFloodService object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Database\Connection $database
   *   The Database connection.
   */
  public function __construct(
    AccountProxyInterface $currentUser,
    ConfigFactoryInterface $configFactory,
    TimeInterface $time,
    Connection $database
  ) {
    $this->currentUser = $currentUser;
    $this->configFactory = $configFactory;
    $this->time = $time;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUserFlood(AccountInterface $user = NULL) {
    $limit_count = $this->getUserLimitCount($user);
    $current_count = $this->getUserCurrentCount($user);
    // 0 means no limit.
    if ($limit_count === 0 || $current_count === 0) {
      return FALSE;
    }

    return $current_count >= $limit_count;
  }

  /**
   * Gets flood protection info for the current user, based on their roles.
   *
   * Each role in the system can be assigned flood protection settings. Roles
   * are also assigned a priority. This function fetches the flood protection
   * data for the highest prioirty role that the current user has.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user to get information about.
   *
   * @return array
   *   An array of flood protection data for the current user based on their
   *   roles.
   */
  private function getFloodProtectionInfo(AccountInterface $user) {
    if (empty($this->configData[$user->id()])) {
      $this->configData[$user->id()] = [
        'limit' => 0,
      ];

      $user_roles = $user->getRoles();
      $role = array_intersect_key($this->getRoleInfo(), array_flip($user_roles));
      $role_id = key($role);

      $config = $this->configFactory->get('private_message_flood.role.' . $role_id);
      if ($config) {
        $this->configData[$user->id()] = $config->get();
      }
    }
    return $this->configData[$user->id()];
  }

  /**
   * Fetches an array of roles, ordered by priority.
   *
   * Each role in the system may have flood protection settings that determine
   * how many posts a user can make in a given duration. Roles have priority,
   * and the highest priority role is the one that a user is checked against
   * when determining whether or not they have crossed the threshold for their
   * flood settings.
   *
   * @return array
   *   An array of roles that have flood protection assigned to them, keyed by
   *   the role ID, with the value being the weight. Roles in the array are
   *   ordered highest to lowest priority.
   */
  private function getRoleInfo() {
    $roles = array_map(['\Drupal\Component\Utility\Html', 'escape'], user_role_names(TRUE));
    $items = [];
    foreach (array_keys($roles) as $role_id) {
      $role_info = $this->configFactory->get('private_message_flood.role.' . $role_id)->get();
      if (!empty($role_info)) {
        $items[$role_id] = $role_info['weight'];
      }
    }
    uasort($items, [$this, 'sortByWeight']);

    return $items;
  }

  /**
   * Sorting callback to sort arrays by their 'weight' attribute.
   *
   * @param mixed $a
   *   Comparable value a.
   * @param mixed $b
   *   Comparable value b.
   *
   * @return int
   *   1 if a is bigger.
   *   0 if they're equal.
   *   -1 if b is bigger.
   */
  private function sortByWeight($a, $b) {
    if ($a === $b) {
      return 0;
    }

    return $a > $b ? 1 : -1;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserLimitCount(AccountInterface $user = NULL) {
    if (empty($user)) {
      $user = $this->currentUser;
    }
    $config = $this->getFloodProtectionInfo($user);
    return isset($config['limit']) ? $config['limit'] : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserCurrentCount(AccountInterface $user = NULL) {
    if (empty($user)) {
      $user = $this->currentUser;
    }
    $limit_type = $this->getUserLimitType($user);
    $limit_duration = $this->getUserLimitDuration($user);
    $count = 0;

    if ($limit_type == 'thread') {
      // Query the number of posts that the user has made since the given
      // timestamp.
      $query = $this->database->select('private_message_threads', 'pm');
    }
    elseif ($limit_type == 'post') {
      // Query the number of posts that the user has made since the given
      // timestamp.
      $query = $this->database->select('private_messages', 'pm');
    }
    if (isset($query)) {
      try {
        // Get the amount of time in which they are limited to make said number
        // of threads/posts.
        $duration = new DateInterval($limit_duration);
        // Get the current time.
        $now = new DateTime();
        $now->setTimestamp($this->time->getRequestTime());
        // Get the UNIX timestamp for time that is $duration ago from now. For
        // example, if $duration is one year, then $since will be the exact date
        // and time one year ago from now.
        $since = $now->sub($duration)->format('U');

        $post_count = $query->fields('pm', ['id'])
          ->condition('owner', $user->id())
          ->condition('created', $since, '>=')
          ->countQuery()
          ->execute()
          ->fetchField();
        $count = $post_count;
      }
      catch (Exception $e) {
        // DateInterval can throw an exception.
        watchdog_exception('private_message_flood', $e);
      }
    }
    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserLimitType(AccountInterface $user = NULL) {
    if (empty($user)) {
      $user = $this->currentUser;
    }
    $config = $this->getFloodProtectionInfo($user);
    return isset($config['type']) ? $config['type'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getUserLimitDuration(AccountInterface $user = NULL) {
    if (empty($user)) {
      $user = $this->currentUser;
    }
    $config = $this->getFloodProtectionInfo($user);
    return isset($config['duration']) ? $config['duration'] : 0;
  }

}
