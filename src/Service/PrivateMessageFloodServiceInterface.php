<?php

namespace Drupal\private_message_flood\Service;

use Drupal\Core\Session\AccountInterface;

/**
 * Provides services for the Private Message Flood Protection module.
 */
interface PrivateMessageFloodServiceInterface {

  /**
   * Determines whether a user has reached their private message post limit.
   *
   * Flood limits are set per role. This function will find the lowest weighted
   * role that the user currently holds, and retrieve the flood limit values for
   * that role. It will then check if a flood limit has been set for said role,
   * and check if the user has gone over the limit. Flooding can be set either
   * to limit the number of posts per duration, or the number of threads per
   * duration. This function checks whichever has been set.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The user, default to current.
   *
   * @return bool
   *   A boolean indicating whether or not they have reached their flood limit.
   *   TRUE means they have reached the limit, and should be stopped from
   *   posting. FALSE means they can post as normal.
   */
  public function checkUserFlood(AccountInterface $user = NULL);

  /**
   * Retrieve the limit the user has.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The user, default to current.
   *
   * @return int
   *   The number allowed for the user (based on role).
   */
  public function getUserLimitCount(AccountInterface $user = NULL);

  /**
   * Retrieve the number already used within the current duration.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The user, default to current.
   *
   * @return int
   *   The number used done by the user (based on role).
   */
  public function getUserCurrentCount(AccountInterface $user = NULL);

  /**
   * Retrieve the limit type for the user.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The user, default to current.
   *
   * @return string
   *   The type of limitations for the user (based on role).
   */
  public function getUserLimitType(AccountInterface $user = NULL);

  /**
   * Retrieve the limit duration for the user.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $user
   *   The user, default to current.
   *
   * @return int
   *   The duration time of the limit window for the user (base don role).
   */
  public function getUserLimitDuration(AccountInterface $user = NULL);

}
